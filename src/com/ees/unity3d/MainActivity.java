package com.ees.unity3d;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.ees.bluetooth.BTFacade;
import com.ees.bluetooth.ConnectionState;
import com.ees.unitybluetooth.R;

public class MainActivity extends Activity {
	
	private static final String TAG = "Unity3DBTActivity";
	
	TextView currState;
	EditText devs;
	EditText devNum;
	EditText devConnName;
	List<String> pairedDevices = new ArrayList<String>();
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if(resultCode == RESULT_OK) {
			Log.v(TAG, "User pressed OK, lets use Bluetooth.");
			//bt.start();
		} else {
			Log.v(TAG, "User pressed cancel, Bluetoonth will not be turned on.");
		}
	}

	BTFacade bt;
	

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        bt = new BTFacade(MainActivity.this);
        
        
        currState = (TextView) findViewById(R.id.currState);
        devs = (EditText) findViewById(R.id.editText1);
        devNum = (EditText) findViewById(R.id.devNumEText);
        devConnName = (EditText) findViewById(R.id.devNameEText);
      
        // Discovery
        final Button discoveryButton = (Button) findViewById(R.id.discoveryButton);
        discoveryButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				bt.enableDiscoverable(100);				
			}
		});
        
        // Scan
        final Button scanButton = (Button) findViewById(R.id.scanButton);
        scanButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				//bt.enableDiscoverable(300);
				bt.startDiscovery();
			}
		});
        
        // Stop
        final Button stopButton = (Button) findViewById(R.id.stopButton);
        stopButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				bt.disconnect();				
			}
		});
        
        // Devices
        final Button devButton = (Button) findViewById(R.id.devicesButton);
        devButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				pairedDevices.clear();
				devs.setText("");
				int i=0;
				if (bt.getPairedDevices()!=null) {
					for (String item : bt.getPairedDevices()) {
						Log.d(TAG, "Paired devName: " + item);
						pairedDevices.add(item);
						devs.append(i + ". " + item + "\n");
						i++;
					}
				} else {
					Log.i(TAG, "No paired devices.");
				}
				
				if (bt.getNewDevices()!=null) {
					for (String item : bt.getNewDevices()) {
						Log.d(TAG, "New devName: " + item);
						pairedDevices.add(item);
						devs.append(i + ". " + item + "\n");
						i++;
					}
				} else {
					Log.i(TAG, "No new devices.");
				}
			}
		});
        
        // Connect
        final Button connectButton = (Button) findViewById(R.id.connectButton);
        connectButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				int num = Integer.parseInt(devNum.getText().toString());
				Log.d(TAG, "Try connect to dev num: " + num + ", devName: " + pairedDevices.get(num));
				bt.connect(pairedDevices.get(num));				
			}
		});
        
        // State
        final Button stateButton = (Button) findViewById(R.id.stateButton);
        stateButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				devConnName.setText(bt.getConnectedDevName());
				currState.setText(decodeConnState(bt.getState()));
			}
		});
        
        //Server
        final Button serverButton = (Button) findViewById(R.id.serverButton);
        serverButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				bt.startServer();
			}
		});
        
    }
    
    
    @Override
	protected void onDestroy() {
		super.onDestroy();
		bt.disconnect();
	}
    
    private String decodeConnState(int state) {
    	if(state == ConnectionState.CONNECTED) return "CONNECTED";
    	if(state == ConnectionState.CONNECTING) return "CONNECTING";
    	if(state == ConnectionState.LISTENING) return "LISTENING";
    	if(state == ConnectionState.NONE) return "NONE";
		return null;
    }
    
}
